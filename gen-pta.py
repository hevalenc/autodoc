import datetime
import docx
from docx.shared import Inches, Pt, Cm
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.enum.table import WD_CELL_VERTICAL_ALIGNMENT, WD_TABLE_ALIGNMENT

linea = "Nequi"

doc = docx.Document()

records = (
    ('AUDITORIA', 'CGST200XX - CIBERSEGURIDAD'),
    ('AUDITOR(ES)', 'EDWARD ANTONIO NARANJO GUZMAN\nAUDITOR 2'),
    ('PRUEBA', 'PRUEBAS DE SEGURIDAD SOBRE ' + linea.upper()),
    ('FECHA', datetime.date.today().strftime("%d-%m-%Y"))
)

style = doc.styles['No Spacing']
style.font.name = 'Arial'
style.font.size = Pt(11)

table = doc.add_table(rows=4, cols=2, style='Table Grid')
table.alignment = WD_TABLE_ALIGNMENT.CENTER
table.autofit = False

"""
https://python-docx.readthedocs.io/en/latest/dev/analysis/features/table/table-props.html

https://stackoverflow.com/questions/43051462/python-docx-how-to-set-cell-width-in-tables
https://groups.google.com/forum/#!topic/python-docx/o5b7BiG_cKs
"""

i = 0
for a,b in records:
	c1 = table.rows[i].cells[0].paragraphs[0].add_run(a)
	table.rows[i].cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.CENTER
	table.rows[i].cells[0].width = Cm(4)
	c1.bold = True
	c1.font.name = 'Arial'
	c2 = table.rows[i].cells[1].paragraphs[0].add_run(b)
	table.rows[i].cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
	table.rows[i].cells[1].width = Cm(10)
	c2.font.name = 'Arial'
	i = i + 1

p = doc.add_paragraph()
p.style = doc.styles['No Spacing']
p.add_run('\n')

### 1st paragraph - Proposito ###
# Title
p = doc.add_paragraph('', style='List Number')
title = p.add_run('PROPÓSITO DE LA PRUEBA')
title.bold = True
p.style.font.name = 'Arial'
# Body
obj_general = "Generar confianza en el ambiente de ciberseguridad del "\
"Grupo Bancolombia a través de la investigación, análisis, explotación "\
"y documentación de vulnerabilidades y debilidades presentes en " + linea + ".\n"
p = doc.add_paragraph(obj_general)
p.style = doc.styles['No Spacing']

### 2 - Alcance ###
# Title
p = doc.add_paragraph('', style='List Number')
title = p.add_run('ALCANCE')
title.bold = True
p.style.font.name = 'Arial'
# Body
body = "Las pruebas de seguridad fueron realizadas sobre"\
" las siguientes superficies de exposición:\n"
p = doc.add_paragraph(body)
p.add_run('\n')
superficies = ["dominio1", "dominio2", "dominio3", "dominio4"]
for item in superficies:
	p.add_run(item + '\n')
p.style = doc.styles['No Spacing']

### 3 - Analisis ###
# Title
p = doc.add_paragraph('', style='List Number')
title = p.add_run('ANALISIS EJECUTADO')
title.bold = True
p.style.font.name = 'Arial'
# Body
body = "Los siguientes son detalles de las pruebas de seguridad"\
" realizadas sobre los dominios mencionados anteriormente:"
p = doc.add_paragraph(body)
p.add_run('\n')
p.style = doc.styles['No Spacing']

### 4 - Testing ###
# Title
p = doc.add_paragraph('', style='List Number')
title = p.add_run('DESARROLLO DE LAS PRUEBAS')
title.bold = True
p.style.font.name = 'Arial'
# Body
body = "Primero se empieza con un escaneo..."
p = doc.add_paragraph(body)
p.add_run('\n')
p.style = doc.styles['No Spacing']

### 5 - Conclusion ###
# Title
p = doc.add_paragraph('', style='List Number')
title = p.add_run('CONCLUSIÓN')
title.bold = True
p.style.font.name = 'Arial'
# Body
conclusiones = ['observacion 1', 'observacion 2', 'observacion 3']
body = 'Se realizaron las pruebas respectivas se encontraron '\
'las siguientes observaciones:\n'
p = doc.add_paragraph(body)
p.style = doc.styles['No Spacing']
for conclus in conclusiones:
	p = doc.add_paragraph(conclus, style='List Bullet')
	p.style.font.name = 'Arial'
	p.style.font.size = Pt(11)
p.add_run('\n')
### To do: Build table with findings ###

doc.save('papel-trabajo.docx')
