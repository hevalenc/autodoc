import docx
import datetime
from docx.shared import Inches, Pt
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.enum.text import WD_BREAK

"""
Cambiar el nombre de linea (no importa que esté en miníscula)
Dejar los dominios separados solamente por comas y nada más
Documento final = INFF-LINEA.docx

Personalizar si se desea, el código es libre
y abierto para ser modificado como se desee

Author: Henry Valencia

Ejemplo:
linea = banistmo
dominios = "dom1.banistmo,dom2.banistmo,dom3.banistmo,dom4.banistmo,dom5.banistmo"
"""

linea = "nequi"
dominios = "apigwpa.nequi.com.pa,api.nequi.com,api.sandbox.nequi.com,"\
"authpp.nequi.com,ayuda.nequi.com.co,ayuda.nequi.com.pa,botawspdn.nequi.com.co"\
",cobros.nequi.com.co,conecta.nequi.com,conecta.nequi.com.co,"\
"docs.conecta.nequi.com,docs.conecta.nequi.com.co,media.comunidad.nequi.com,"\
"metidasdeplata.nequi.com.co,negocios.nequi.com.co,nequibot.nequi.com,"\
"nequibot.nequi.com.co,nequi.com,nequi.com.co,nequi.com.pa,oauth.nequi.com"\
",oauth.sandbox.nequi.com,paga.nequi.com.co,recarga.nequi.com.co,tiendas.nequi.com.pa"\
",transacciones.nequi.com,webhooks.nequi.com,webhooks.sandbox.nequi.com"\
",www.nequi.com,www.nequi.com.co,www.nequi.com.pa"

doc = docx.Document()

### Portada ###
doc.add_picture('bancol.png', width=Inches(4.5))
last_paragraph = doc.paragraphs[-1] 
last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER
last_paragraph.add_run('\n\n\n\n')

p = doc.add_paragraph()
p.style.font.size = Pt(20)
p.style.font.name = 'Arial'
obj = p.add_run('VICEPRESIDENCIA DE AUDITORÍA INTERNA BANCOLOMBIA')
obj.bold = True
p.alignment = WD_ALIGN_PARAGRAPH.CENTER
p.add_run('\n\n\n\n')

p = doc.add_paragraph()
p.style.font.size = Pt(20)
p.style.font.name = 'Arial'
obj = p.add_run('EVALUACIÓN\nCIBERSEGURIDAD EXTERNA ' + linea.upper())
obj.bold = True
p.alignment = WD_ALIGN_PARAGRAPH.CENTER
p.add_run('\n\n\n\n')

p = doc.add_paragraph()
p.style.font.size = Pt(20)
p.style.font.name = 'Arial'
obj = p.add_run(str(datetime.date.today()))
obj.bold = True
p.alignment = WD_ALIGN_PARAGRAPH.CENTER
obj.add_break(WD_BREAK.PAGE)

### Objetivo general ###
# Title
p = doc.add_paragraph()
p.style = doc.styles['No Spacing']
title = p.add_run('OBJETIVO GENERAL')
title.bold = True
title.font.size = Pt(14)
p.style.font.name = 'Arial'
p.add_run('\n\n')
# Body
obj_general = "Contribuir con el ambiente de seguridad del Grupo "\
"Bancolombia y sus filiales nacionales e internacionales mediante "\
"la identificación de amenazas externas."
body = p.add_run(obj_general)
body.font.size = Pt(12)
p.add_run('\n')

### Objetivos específicos ###
# Title
p = doc.add_paragraph()
p.style = doc.styles['No Spacing']
title = p.add_run('OBJETIVOS ESPECÍFICOS')
title.bold = True
title.font.size = Pt(14)
p.add_run('\n')
# Body
obj_esp = ["Determinar los enlaces y superficies de exposición en internet del"\
" Grupo Bancolombia y sus filiales nacionales e internacionales",
"Caracterizar los activos de información de la superficie de exposición del"\
" grupo Bancolombia",
"Identificar vulnerabilidades que afecten la seguridad de los activos de"\
" información expuestos en internet",
"Explotar vulnerabilidades identificadas que representen un riesgo para la"\
" seguridad de la información expuesta en internet"]
for item in obj_esp:
	p = doc.add_paragraph(item, style='List Bullet')
	p.style.font.name = 'Arial'
	p.style.font.size = Pt(12)
p.add_run('\n')

### Alcance ###
# Title
p = doc.add_paragraph()
p.style = doc.styles['No Spacing']
title = p.add_run('ALCANCE')
title.bold = True
p.style.font.size = Pt(14)
p.add_run('\n\n')
# Body
alcance = "Se incluyeron los activos de información expuestos a internet"\
" de las líneas de negocio nacionales e internacionales del "\
"Grupo Bancolombia; en " + linea[0].upper() + linea[1:] + " son los siguientes:"
body = p.add_run(alcance)
body.font.size = Pt(12)
p.add_run('\n')
for item in dominios.split(","):
	p = doc.add_paragraph(item, style='List Bullet')
	p.style.font.name = 'Arial'
	p.style.font.size = Pt(12)
p.add_run('\n')

doc.add_page_break()

### Conclusion ###
# Title
p = doc.add_paragraph()
p.style = doc.styles['No Spacing']
title = p.add_run('CONCLUSIÓN')
title.bold = True
p.style.font.size = Pt(14)
p.add_run('\n\n')
# Body
conclusion = "Los activos tecnológicos de información de "\
 + linea[0].upper() + linea[1:] + " expuestos en internet"\
" , cuentan con niveles de seguridad adecuados para la respuesta"\
"  a posibles amenazas provenientes de atacantes externos, se"\
" validaron los controles de seguridad básicos de los dominios que "\
"exponen contenido a la web y se obtuvo resultado "
body = p.add_run(conclusion)
resul = p.add_run("satisfactorio")
resul.bold = True
body2 = p.add_run(".")
body.font.size = Pt(12)
resul.font.size = Pt(12)
body2.font.size = Pt(12)
p.add_run('\n')

### TO DO ###
### Create PDF file and attach Andre's signature ###

doc.save('INFF-' + linea.upper() + '.docx')
