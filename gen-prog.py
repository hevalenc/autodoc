import docx
from docx.shared import Inches, Pt
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.enum.text import WD_COLOR_INDEX

evaluacion = "Ciberseguridad Canales"
sujeto = "los diferentes canales transaccionales"
alcance = ["App Personas, Módulo de pagos: Facturas",
		   "App Personas, Módulo de pagos: Billetera Móvil",
		   "Lectura, Generación y Transacciones QR",
		   "App ALM: Ahorro a la Mano",
		   "Kioskos"]

doc = docx.Document()

# Adding header
header = doc.sections[0].header.paragraphs[0]
p = header.add_run()
p.add_picture('bancolombia.png', width=Inches(2))
p.alignment = WD_ALIGN_PARAGRAPH.RIGHT
p = header.add_run('\n')
p = header.add_run('Auditoria de Ciberseguridad e Infraestructura\n\n')
p.bold = True
header.style.font.name = 'Arial'
header.style.font.size = Pt(10)
#document.add_picture('logo-bancolombia.png', width=Inches(1.50))

# Setting document style
style = doc.styles['No Spacing']
style.font.name = 'Arial'
style.font.size = Pt(11)

# First Paragraph : First title
p = doc.add_paragraph()
#p.add_run('\n')
p.add_run(evaluacion).bold = True
p.style = doc.styles['No Spacing']
p.runs[0].add_break(docx.enum.text.WD_BREAK.LINE)

# Second Paragraph: Objetivo general
p = doc.add_paragraph()
p.style = doc.styles['No Spacing']
obj = p.add_run('\n')

# Objetivo General
p.add_run('Objetivo General').bold = True
p.add_run('\n')

obj_general = "Generar confianza en el ambiente de ciberseguridad"\
" del Grupo Bancolombia a través de la investigación"\
", análisis, explotación y documentación de"\
" vulnerabilidades y debilidades presentes en " + sujeto + '.'

p.add_run(obj_general)
p.add_run('\n\n')

# Third Paragraph: Objetivos especificos
p.add_run('Objetivos Especificos').bold = True

objs_esp = ['Realizar pruebas de autenticacion y manejo de sesion sobre'\
' cada uno de los canales mencionados',
'Analizar debilidades en las funcionalidades'\
' de cada aplicación de los canales',
'Llevar a cabo inyecciones de datos sobre los campos de las aplicaciones',
'Explotar las vulnerabilidades encontradas de manera controlada,'\
' sin afectar la disponibilidad'\
' e integridad de la información de alguno de los canales',
'Documentar los ataques realizados y observaciones encontradas']

for item in objs_esp:
	p = doc.add_paragraph(item, style='List Bullet')
	p.style.font.name = 'Arial'
	p.style.font.size = Pt(11)

# Fourth Paragraph: Alcance
p = doc.add_paragraph()
p.style = doc.styles['No Spacing']
obj = p.add_run('Alcance')
obj.bold = True

p_alcance = 'Las pruebas de seguridad se realizaran bajo los escenarios'\
' de un atacante externo sin acceso sobre cada una de las superficies'\
' mencionadas a continuacion:'

p.add_run(p_alcance)
p.runs[0].add_break(docx.enum.text.WD_BREAK.LINE)

for item in alcance:
	p = doc.add_paragraph(item, style='List Bullet')
	p.style.font.name = 'Arial'
p.runs[0].add_break(docx.enum.text.WD_BREAK.LINE)

# Fifth Paragraph: Pruebas
p = doc.add_paragraph()
p.style = doc.styles['No Spacing']
obj = p.add_run('Ejecución')
obj.bold = True
p.add_run('\n')
p.add_run('El detalle de las pruebas a realizar se encuentran '\
	'en los documentos anexos:\n\n')
msg = p.add_run('Agregar en esta seccion los arboles de ataque que'\
	' entreguen un detalle de las pruebas a realizar')
msg.italic = True
msg.font.highlight_color = WD_COLOR_INDEX.YELLOW

p.add_run('\n')

# Seventh Paragraph: Antecedentes
p = doc.add_paragraph()
p.style = doc.styles['No Spacing']
obj = p.add_run('Antecedentes')
obj.bold = True
p.add_run('\n')
p.add_run('No se tienen en cuenta antecedentes para el desarrollo de la evaluación.\n')

# Seventh Paragraph: Encargados
p = doc.add_paragraph()
p.style = doc.styles['No Spacing']
obj = p.add_run('Auditores a cargo:')
p.add_run('\n')
msg = p.add_run('Auditor 1\nAuditor 2\n')
msg.font.highlight_color = WD_COLOR_INDEX.YELLOW

# Saving document
doc.save('Programa-de-trabajo.docx')
